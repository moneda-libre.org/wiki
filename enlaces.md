el documento fuente parte de enlaces recopilados de josepeuta en canal telegram de moneda lliure cat


## Telegram

- Chat G1 España: @G1monedalibre
- Canal G1 (en castellano): @g1canal
- Chat G1 France: https://t.me/monnaielibrejune
- Chat G1 Portugal: https://t.me/g1moedalivre
- Chat Cono Sur (Argentina y más): https://t.me/joinchat/rmHNfLC80IJkN2Ux
- Chat G1 Latino America: https://t.me/joinchat/-25Hfr0lRekwYTA0
- OfftopicsG1castellano (Chat para hablar de cualquier cosa alrededor de la moneda libre G1 y otras): https://t.me/offtopicG1_cast
- Canal AnunciosG1 España: https://t.me/AnunciosG1ESP

## WEBS

- Web española: https://foro.moneda-libre.org/
- Web catalana: https://monedalliure.org/
- Web Francesa: https://forum.monnaie-libre.fr/
- Web portuguesa: https://moedalivreportugal.mn.co/feed
- Web para intercambios G1
  - http://gchange.es
  - http://gchange.fr
  - https://girala.net/ (comercios y servicios profesionales en g1)
- Web G1 en Esperanto https://komun.org/blog/eo/g1-libera-mono

## G1 en Inglés

- Mastodon account: https://mastodon.social/@librecurrency
- twitter account: https://twitter.com/LibreCurrency
- facebook page: https://www.facebook.com/G1LibreCurrency/
- facebook group: https://www.facebook.com/groups/235459317067168/?ref=share
