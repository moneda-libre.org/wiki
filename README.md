# wiki

Sintetizando el contenido de foro.moneda-libre.org i demás lugares

Para personas usuarias:

* https://monedalliure.org (**Web informativa**)
* https://foro.moneda-libre.org (**Foro peninsular**)
* https://cesium.app (Web para descargar la **aplicación móvil**)
* https://www.gchange.es (_Gchange_: sitio web de **oferta y demanda en G1**)
* https://gitlab.com/moneda-libre.org/wiki (**Documentación colaborativa**)

Para personas desarrolladoras-et-al.:

* https://duniter.org/en/wiki/duniter/install/#ubuntudebian-64-bits (como añadir **nodos duniter**)
