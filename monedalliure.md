# Info 11 Maig 2021

Estat actual del domini i servidors relacionats amb MonedaLliure.org.

    Domini · monedalliure.org 1:

El gestiona sheveck a través de DonDominio

    Portal web en català · monedalliure.org 1:

Està en un servidor de LaLoka, administrat per @buttle. Qui ha traduit i té accés al portal d’administració sóc jo. El software utilitzat és un Grav.

    Node de Duniter: duniter00.monedalliure.org

Està en un servidor de LaLoka, administrat per @buttle. Qui s’encarrega de mantenir-lo són el @buttle, el @kapis i crec que el @pedrog1 . Si algú vulgués instal·lar un node de duniter a un altre servidor podriem redirigir duniter01.monedalliure.org, duniter02 o el que calgui.

:link: Estat duniter00.monedalliure.org

    Peertube: video.monedalliure.org

De moment està a la raspberry de sheveck amb yunohost, però es comenta que estaria bé posar-lo a un lloc més estable i potent. Fins ara no ha donat masses problemes però pot ser un bon moment per migrar-ho a un nou servidor.

Documents relacionats:

* https://arxius.laloka.org/w/projectes/#MonedaLliure
