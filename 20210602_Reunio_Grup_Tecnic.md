Reunió Grup Suport Tècnic G1 (et al) 
https://meet.guifi.net/g1catalunya
================================
pad de l'anterior 21 de març de 2021 https://pad.laloka.org/p/g1-210321


Assistents
- Chris
- Carles
- Xavier
- Kapis
- Pedro
- Simó

part divulgativa

* web cara enfora: 
    * monedalliure.org
        * https://monedalliure.org/futursmembres - per veure quan s'aproven els nous membres (certificacions pendents, distànccia, ...)
    * moneda-libre.org
* peertube: tube.p2p.legal (Instància Poka)

Eines:

* forum: foro.moneda-libre.org (manteniment Poka) https://play.google.com/store/apps/details?id=com.discourse&hl=en&gl=US
* girala.net
* gchange.es
* liberaforms: https://gitlab.com/liberaforms/liberaforms
* geconomicus
* https://gitlab.com/la-loka/geconomicus
* https://genomicus.dev.laloka.org/
* odoo https://github.com/odoo/odoo https://odoo-community.org / tryton https://tryton.org (més community driven)

Necessitats:

* Recursos escassos per al node duniter actual (corrent sobre servidor de exo)
* nodes de duniter (augmentar resiliència)
* Un lloc on agregar la documentació per a usuaris de forma fàcil (diferents casos d'ús): 
    * wiki.monedalliure.org
        * Fins ara s'havia sugerit emprar una pàgina del gitlab anomenada "Wiki": https://gitlab.com/moneda-libre.org/wiki
        * usuaris finals
        * desenvolupadors
     * Canal de comunicació emprable des de mòbil de forma fàcil:
         * Fòrum?
         * Element/Matrix bridge amb Telegram?
     * forum.moneda-libre.org mode llista de correu, poka no contesta: https://foro.moneda-libre.org/t/modo-lista-correo-permitir-responder-por-correo/276
     * futurs membres (duniter) https://monedalliure.org/futursmembres (no es sincronitza des del 28 de maig 21) podriem tenir-lo al nostre server)
     * Si algué té espai VPS es pot instalar el https://git.duniter.org/nodes/typescript/modules/duniter-currency-monit  per a no dependre de eines desincronitzades.
* afegir interfície guifi en duniter00 (VM eXO), ho mirem Chris i Pedro a https://meet.guifi.net/g1catalunya el 2021-6-5 11:00

Eines: https://duniter.fr/ecosysteme/

Discussió sobre eines de documentació

xavi: que sigui fàcil i que qualsevol usuari la pugui fer anar. https://gitlab.com/moneda-libre.org/wiki

pedro: que sigui resilient (git o similar) per tal de que hi hagi diverses còpies de la informació en diversos servidors

kapis: 

guillem:

proposa: monedalliure : wiki de nous membres

wiki.monedalliure.org : documentació amb més detall (tècnica)

App per a seguir el foro: https://play.google.com/store/apps/details?id=com.discourse&hl=en&gl=US

Crowdfunding estatal actual: 5WHd5RTmQ9QYxmHgQUznggiq5p2BDJtYyyY5DvjGKfaw

Propera reunió: Dc 16 Juny 21:30, aquí:
https://meet.guifi.net/g1catalunya

Log Chat:
```
kapis
coopdevs
20:46
Chris
https://gitlab.com/liberaforms/liberaforms
20:50
kapis
https://github.com/odoo/odoo/blob/14.0/LICENSE
20:51
pedro
també està OCA https://odoo-community.org/
The Odoo Community Association, or OCA, is a nonprofit organization whose mission is to promote the widespread use of Odoo and to support the collaborative development of Odoo features.
20:52
Chris
pad.laloka.org/p/g1-2100602
20:59
kapis
https://duniter00.monedalliure.org/network/peering
https://monit.g1.nordstrom.duniter.org/willMembers?lg=fr&pending=&mode=undefined
https://monit.g1.nordstrom.duniter.org/about?lg=fr
https://git.duniter.org/nodes/typescript/modules/duniter-currency-monit
21:10
pedro
pot ser que algú altre el tingui i canviem l'enllaç?
a part de nordstrom
21:13
kapis
https://duniter.fr/ecosysteme/
21:15
pedro
+1 xavi
aquests missatges són molt pesats hhaa
21:18
Guillem
https://monedalliure.org/participar
21:27
pedro
https://gitlab.com/moneda-libre.org/wiki
21:29
Guillem
monedalliure.org
21:36
pedro
ok kapis. futurs membres és un resum de la documentació completa-detallada (a part de la seva secció tècnica)
i si wordpress facilita, ok
21:42
kapis
https://duniter.fr/ecosysteme/
21:45
pedro
kapis, lo de poka i llista de correu per a discourse... pots fer update?
21:46
kapis
pareix que no ha respost publicament... preguntali directament per PM en francés... crec que no sap español
castellà
21:47
pedro
pots contactar-lo tu? 😃
21:47
kapis
hahahha, nooo, q no controle tant la part técnica que fa falta fer i tu li ho explicaràs millor
https://www.deepl.com/translator
xD
21:47
pedro
parla anglès?
21:48
kapis
si... segurament... ell està desenvolupament el gecko un nou client mes facil q cesium: https://forum.monnaie-libre.fr/u/poka/summary
21:49
pedro
il parle anglais?
😄
21:49
kapis
ací tens al Poka(foros i Gecko) i el Elois(duniter) https://tube.p2p.legal/videos/watch/288e9949-a836-42d4-8885-80601ab89eda
https://play.google.com/store/apps/details?id=com.discourse&hl=en&gl=US
21:58
```
